package piservice

import (
	"encoding/json"
	"fmt"
	"net/http"
	"strconv"

	"github.com/julienschmidt/httprouter"
)

func InitializeRoutes(router *httprouter.Router) {
	router.POST("/api/v1/pi_job/", createJobEndpoint)
	router.GET("/api/v1/pi_job/:id/download", downloadJobEndpoint)
	router.GET("/api/v1/pi_job/:id", getJobEndpoint)
}

func createJobEndpoint(w http.ResponseWriter, r *http.Request, _ httprouter.Params) {
	jobPayload := Job{}
	json.NewDecoder(r.Body).Decode(&jobPayload)

	job := CreateJob(jobPayload.Num)
	response := CreateJobResponse{JobId: job.JobId, Success: true}

	responseJson, _ := json.Marshal(response)

	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(201)

	fmt.Fprintf(w, "%s", responseJson)
}

func getJobEndpoint(w http.ResponseWriter, r *http.Request, ps httprouter.Params) {
	id := ps.ByName("id")
	intId, _ := strconv.Atoi(id)
	job := GetJobById(intId)
	jobJson, _ := json.Marshal(job)
	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(200)
	fmt.Fprintf(w, "%s", jobJson)
}

func downloadJobEndpoint(w http.ResponseWriter, r *http.Request, ps httprouter.Params) {
	id := ps.ByName("id")
	rootPath := GetConfiguration().ZipPath
	fileName := "digits_of_pi_" + id + ".zip"
	path := rootPath + fileName
	w.Header().Set("Content-Type", "application/zip")
	w.Header().Set("Content-Disposition", "attachment; filename="+fileName)
	http.ServeFile(w, r, path)
}
