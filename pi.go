package piservice

import (
	"archive/zip"
	"bufio"
	"fmt"
	"io"
	"math"
	"math/big"
	"os"
	"strconv"
)

func CalcPI(Num int) *big.Rat {
	K := 6
	M := new(big.Rat).SetFloat64(1.0)
	L := new(big.Rat).SetFloat64(13591409.0)
	X := new(big.Rat).SetFloat64(1.0)
	S := new(big.Rat).SetFloat64(13591409.0)

	for i := 1; i <= Num; i++ {
		p1 := new(big.Rat).SetFloat64((math.Pow(float64(K), 3) - float64(K<<4)))
		p2 := new(big.Rat).SetFloat64(math.Pow(float64(i), 3))
		p3 := M.Mul(M, p1)
		M = p3.Quo(p3, p2)
		L = L.Add(L, new(big.Rat).SetFloat64(545140134.0))
		X = X.Mul(X, new(big.Rat).SetFloat64(-262537412640768000.0))
		TS := M.Mul(M, L)
		S = S.Add(TS.Quo(TS, X), S)
		K += 12
	}

	f1 := new(big.Rat).SetFloat64(426880.0)
	f2 := f1.Mul(f1, new(big.Rat).SetFloat64(math.Sqrt(10005)))
	pi := f2.Quo(f2, S)

	return pi
}

// Calculates PI digits asynchronously.
// Starts by setting the job to the Processing state, once it's done it creates
// a file with the result of the calculation and zips it
// TODO: Break in smaller functions and improve error checking
func CalcPIAsync(jobId int, num int) {
	db := GetDatabaseConnection()

	tx, err := db.Begin()
	if err != nil {
		fmt.Println(err)
	}
	stmt, err := tx.Prepare("UPDATE jobs SET status=? WHERE id=?")
	if err != nil {
		fmt.Println(err)
	}
	defer stmt.Close()

	_, err = stmt.Exec(Processing, jobId)
	if err != nil {
		fmt.Println(err)
	}
	tx.Commit()

	fmt.Println("job ", jobId, "started to calculate ", num)
	_ult := CalcPI(num)
	fmt.Println("job ", jobId, "finished ", _ult.FloatString(num))

	a := _ult.FloatString(num)

	f, err := os.Create("/tmp/digits_of_pi_" + strconv.Itoa(jobId) + ".txt")
	if err != nil {
		fmt.Println(err)
	}

	w := bufio.NewWriter(f)
	fmt.Fprintf(w, "%v\n", a)
	w.Flush()
	f.Close()

	f2, err := os.Create("/tmp/digits_of_pi_" + strconv.Itoa(jobId) + ".zip")
	if err != nil {
		fmt.Println(err)
	}

	defer f2.Close()
	zipWriter := zip.NewWriter(f2)

	defer zipWriter.Close()

	zipfile, err := os.Open(GetConfiguration().ZipPath + "digits_of_pi_" + strconv.Itoa(jobId) + ".txt")
	if err != nil {
		fmt.Println(err)
	}

	defer zipfile.Close()
	info, _ := zipfile.Stat()
	header, _ := zip.FileInfoHeader(info)
	writer, _ := zipWriter.CreateHeader(header)
	_, _ = io.Copy(writer, zipfile)

	tx, err = db.Begin()
	if err != nil {
		fmt.Println(err)
	}

	stmt, err = tx.Prepare("UPDATE jobs SET status=? where id=?")
	if err != nil {
		fmt.Println(err)
	}
	defer stmt.Close()

	_, err = stmt.Exec(Complete, jobId)
	if err != nil {
		fmt.Println(err)
	}
	tx.Commit()
}
