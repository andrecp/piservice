package main

import (
	"flag"
	"fmt"
	"log"
	"net/http"
	"os"

	"github.com/julienschmidt/httprouter"
	"gitlab.com/andrecp/piservice"
)

func main() {
	if len(os.Args) == 1 {
		fmt.Println("usage: piservice <command> [<args>]")
		return
	}

	// Serve subcommand
	serveCommand := flag.NewFlagSet("serve", flag.ExitOnError)

	// Configuration
	port := os.Getenv("PORT")
	zipPath := os.Getenv("ZIP_PATH")
	databasePath := os.Getenv("DATABASE_PATH")

	switch os.Args[1] {
	case "serve":
		serveCommand.Parse(os.Args[2:])
		if serveCommand.Parsed() {
			// Bootstrap
			config := piservice.CreateConfiguration(port, zipPath, databasePath)
			db := piservice.CreateDatabaseConnection(config.DatabasePath)
			defer db.Close()

			// Start service
			router := httprouter.New()
			piservice.InitializeRoutes(router)
			log.Fatal(http.ListenAndServe(":"+config.Port, router))
		}

	default:
		fmt.Printf("%v is not valid command.\n", os.Args[1])
		os.Exit(2)
	}
}
