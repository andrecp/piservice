# Pi Service

Pi service can calculate Pi to the n-th digit and return you the result!

## Architecture

piservice is written in Go and distributed as a single binary.

Go presents some advantages for the piservice, including:
  1. Easy to install, just a single binary that you have to run;
  2. Goroutines make it is trivial to handle asynchronous processing of Pi digits;
  3. Minimum resources footprint usage.

piservice runs a web service with a REST interface at port `PORT` for creating and querying PI calculation jobs.

It uses a SQLite3 database at `DATABASE_PATH` to store the state of its jobs.

SQLite3 was chosen because it doesn't require any installation and will be available in any modern Linux server.

A zip file containing an UTF-8 encoded text file with the result of the calculation is created asychronously by the Goroutines at `ZIP_PATH`.

## Developing

If you want to be a developer in piservice you need to install Vagrant and a provider like VirtualBox. Then:

```
git clone https://gitlab.com/andrecp/piservice.git
cd piservice
vagrant up
```

Now you will have a VM built with a Go environment and your project folder sync'd.

```
vagrant ssh
go install ./...
piservice serve
```

### Project layout

```
-- Vagrantfile  # Creates a Vagrant VM
-- bootstrap.sh  # Installs Go inside Vagrant
-- models.go  # Definition of the models used in this project, for example, Job.
-- routes.go  # Handles HTTP paths to Go functions
-- pi.go  # Business logic around pi calculation
-- piservice/main.go  # Binary
-- Godeps  # godeps project, similar to pip or ruby gems
-- vendor/  # project dependencies
```

### Configuration

By default piservice will use the following configuration

```
PORT: 50181
ZIP_PATH: /tmp/
DATABASE_PATH: ./foo.db
```

If an environment variable named after the configuration is set that value will be used instead. For example

```
export PORT=5050
piservice serve
```

Would start piservice on port 5050 instead of 50181.

### Examples

1. Create a job to calculate pi to the 100th digit
```
curl -H "Content-Type: application/json" -X POST -d '{"num":10}' http://localhost:50181/api/v1/pi_job/
```

2. Check the state of job_id 1
```
curl -X GET http://localhost:50181/api/v1/pi_job/1
```

3. Download job_id 1
```
curl -OJ http://localhost:50181/api/v1/pi_job/1/download
```


## Algorithm used

The algorithm used to implement the calculation of Pi was (Chudnvovsky)[https://en.wikipedia.org/wiki/Chudnovsky_algorithm]

## Considerations

### Time

I've chosen to stop working in this project after five hours.

If I had more time I would have implemented better error handling for requests, right now I am ignoring a lot of the possible errors, for example, directory permissions. I am also not complying 100% with the specs because of that, for example, I am not returning success: false and errors OR 404 for GET to non existing jobs.

I would also have implemented a more resilient queue system, right now it is as simple as it gets.

I could have a separated process which would be watching a database for new tasks and picking / executing tasks as they arrive.

The web application would then simply include a task in the database and return to the user.

This would decouple the web app from the queue system allowing them to scale independently.

Unit and integration tests would have been written. I only did simple manual tests.

### Scalability

As I've mentioned earlier, decoupling web app and queue system would allow for better scalability. Using something like PostgreSQL instead of SQLite would also help. As it would splitting the web app in separate VMs (app / db).

As it stands, this project can vertically scale well since Go makes uses of all the cores in a machine and it handles concurrency very well.

But to horizontally scale this project behind a load balancer we would need to make some changes because of the zip file state:
  1. We could have zip files be generated on the fly, this would require recording the result of the Pi calculation in the database. It would also increase the HTTP GET request time to download a zip;
  2. We could have an NFS shared between all the nodes. That way every service would have access to all the files;
  3. We could use an object store to hold the zip file (like S3) and hold a reference to it on the database.

### Containerization

I feel like it's an overkill to create a docker container just to run a binary. If this project was Python or Ruby it would make sense since it would bundle all of it's dependencies in a single image.

This project, in its current state, is self contained apart from SQLite3 and the file system which any modern Linux distro would have available.

### Blue-green deployments

As mentioned in (scalability) it isn't trivial to do blue-green deployments because of the zip files. We would need to keep the idle environment with the file system in sync with production before switching the traffic.

### Stress testing

We could use a tool like apache ab or Locust to load test this service simulating HTTP requests in parallel. This service wasn't stress tested.

### Monitoring

There is no monitoring available. Would use a SaaS to monitor the VM / Application.

### Caching

There is no cache available. We could use NGINX to cache the zip files since they're static and never change.
