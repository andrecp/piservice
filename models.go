package piservice

import (
	"database/sql"
	"fmt"
	_ "github.com/mattn/go-sqlite3"
)

var db *sql.DB
var config Config

const (
	Created    = "created"
	Failed     = "failed"
	Processing = "processing"
	Complete   = "complete"
)

type Job struct {
	JobId  int    `json:"job_id"`
	Status string `json:"status"`
	Num    int    `json:"num"`
}

type CreateJobResponse struct {
	JobId   int    `json:"job_id"`
	Success bool   `json:"success"`
	Error   string `json:"error,omitempty"`
}

type Config struct {
	Port         string
	ZipPath      string
	DatabasePath string
}

func CreateJob(num int) Job {
	job := Job{Num: num, Status: Created}

	tx, err := db.Begin()
	if err != nil {
		fmt.Println(err)
	}

	stmt, err := tx.Prepare("INSERT INTO jobs(status, number) VALUES(?, ?)")
	if err != nil {
		fmt.Println(err)
	}
	defer stmt.Close()

	res, err := stmt.Exec(job.Status, job.Num)
	if err != nil {
		fmt.Println(err)
	}

	tx.Commit()

	jobId, _ := res.LastInsertId()
	job.JobId = int(jobId)

	go CalcPIAsync(job.JobId, job.Num)

	return job
}

func GetJobById(id int) Job {
	job := Job{}

	stmt, err := db.Prepare("SELECT * FROM jobs WHERE id=?")
	if err != nil {
		fmt.Println(err)
	}
	defer stmt.Close()

	rows, err := stmt.Query(id)
	if err != nil {
		fmt.Println(err)
	}
	defer rows.Close()

	for rows.Next() {
		err = rows.Scan(&job.JobId, &job.Status, &job.Num)
		if err != nil {
			fmt.Println(err)
		}
	}

	err = rows.Err()
	if err != nil {
		fmt.Println(err)
	}
	return job
}

func CreateDatabaseConnection(path string) *sql.DB {
	db, _ = sql.Open("sqlite3", path)
	// TODO: This obviously shouldn't be here. In a more mature system we would use something to manage database
	// schema migration.
	bootstrap := "CREATE TABLE IF NOT EXISTS jobs (id INTEGER NOT NULL PRIMARY KEY, status TEXT, number INTEGER);"
	db.Exec(bootstrap)
	return db
}

func GetDatabaseConnection() *sql.DB {
	return db
}

func CreateConfiguration(port, zipPath, databasePath string) Config {
	config = Config{}
	if port == "" {
		port = "50181"
	}
	if zipPath == "" {
		zipPath = "/tmp/"
	}
	if databasePath == "" {
		databasePath = "./foo.db"
	}
	config.Port = port
	config.ZipPath = zipPath
	config.DatabasePath = databasePath
	return config
}

func GetConfiguration() Config {
	return config
}
